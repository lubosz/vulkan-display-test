# Vulkan Acquire Xlib Display test

A small test that showcases the usage of the `VK_EXT_acquire_xlib_display` extension using XCB.

## Dependencies

* xcb
* Xlib
* Vulkan headers and loader
* Vulkan implementation that supports `VK_EXT_acquire_xlib_display`

## Build and run

* `meson build`
* `ninja -C build`
* `VK_INSTANCE_LAYERS=VK_LAYER_KHRONOS_validation ./build/display-test`

## Issues

The small program produces the following false (?) positives in Vulkan validation.

### Validation errors on radv and amdvlk

radv command
```
VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/radeon_icd.x86_64.json VK_INSTANCE_LAYERS=VK_LAYER_KHRONOS_validation ./build/display-test
```

amdvlk command
```
VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/amd_icd64.json VK_INSTANCE_LAYERS=VK_LAYER_KHRONOS_validation ./build/display-test
```

Output
```
UNASSIGNED-Threading-Info(ERROR / SPEC): msgNum: 0 - [ UNASSIGNED-Threading-Info ] Object: 0x10000000001 (Type = 1000002000) | Couldn't find VkDisplayKHR Object 0x10000000001. This should not happen and may indicate a bug in the application.
    Objects: 1
        [0] 0x10000000001, type: 1000002000, name: NULL
UNASSIGNED-Threading-Info(ERROR / SPEC): msgNum: 0 - [ UNASSIGNED-Threading-Info ] Object: 0x10000000001 (Type = 1000002000) | Couldn't find VkDisplayKHR Object 0x10000000001. This should not happen and may indicate a bug in the application.
    Objects: 1
        [0] 0x10000000001, type: 1000002000, name: NULL
UNASSIGNED-Threading-Info(ERROR / SPEC): msgNum: 0 - [ UNASSIGNED-Threading-Info ] Object: 0x10000000001 (Type = 1000002000) | Couldn't find VkDisplayKHR Object 0x10000000001. This should not happen and may indicate a bug in the application.
    Objects: 1
        [0] 0x10000000001, type: 1000002000, name: NULL
UNASSIGNED-Threading-Info(ERROR / SPEC): msgNum: 0 - [ UNASSIGNED-Threading-Info ] Object: 0x10000000001 (Type = 1000002000) | Couldn't find VkDisplayKHR Object 0x10000000001. This should not happen and may indicate a bug in the application.
    Objects: 1
        [0] 0x10000000001, type: 1000002000, name: NULL
VUID-vkDestroyInstance-instance-00629(ERROR / SPEC): msgNum: 0 - [ VUID-vkDestroyInstance-instance-00629 ] Object: 0x55cf884f22a0 (Type = 1) | OBJ ERROR : For VkInstance 0x55cf884f22a0[], VkDisplayKHR 0x10000000001[] has not been destroyed. The Vulkan spec states: All child objects created using instance must have been destroyed prior to destroying instance (https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/vkspec.html#VUID-vkDestroyInstance-instance-00629)
    Objects: 1
        [0] 0x55cf884f22a0, type: 1, name: NULL
```

Furthermore, enumerating the `VkDisplayModeKHR` with `vkGetDisplayModePropertiesKHR` will produce an validation error like this per mode:
```
VUID-vkDestroyInstance-instance-00629(ERROR / SPEC): msgNum: 0 - [ VUID-vkDestroyInstance-instance-00629 ] Object: 0x55e0760432a0 (Type = 1) | OBJ ERROR : For VkInstance 0x55e0760432a0[], VkDisplayModeKHR 0x100000000010[] has not been destroyed. The Vulkan spec states: All child objects created using instance must have been destroyed prior to destroying instance (https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/vkspec.html#VUID-vkDestroyInstance-instance-00629)
    Objects: 1
        [0] 0x55e0760432a0, type: 1, name: NULL

```
