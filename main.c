#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

#include <xcb/xcb.h>
#include <xcb/randr.h>

// Required by Vulkan, since it's using an Xlib interface
#include <X11/Xlib-xcb.h>
#include <X11/extensions/Xrandr.h>

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_xlib_xrandr.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void
_get_mode_properties(VkPhysicalDevice physical_device,
                     VkDisplayKHR display)
{
  uint32_t mode_count;
  VkResult ret = vkGetDisplayModePropertiesKHR(physical_device, display,
                                              &mode_count, NULL);
  assert(ret == VK_SUCCESS);

  printf("Found %d modes\n", mode_count);

  VkDisplayModePropertiesKHR *mode_properties =
    malloc(sizeof(VkDisplayModePropertiesKHR) * mode_count);
  ret = vkGetDisplayModePropertiesKHR(physical_device, display,
                                      &mode_count, mode_properties);
  assert(ret == VK_SUCCESS);
  free(mode_properties);
}

static xcb_randr_output_t
_get_randr_output(xcb_connection_t *connection,
                  xcb_screen_t *screen)
{
  xcb_randr_query_version_cookie_t version_cookie =
      xcb_randr_query_version(connection, XCB_RANDR_MAJOR_VERSION,
                              XCB_RANDR_MINOR_VERSION);
  xcb_randr_query_version_reply_t *version_reply =
      xcb_randr_query_version_reply(connection, version_cookie, NULL);

  assert(version_reply != NULL);

  printf("RandR version %d.%d\n",
         version_reply->major_version, version_reply->minor_version);

  assert(version_reply->major_version >= 1 &&
         version_reply->minor_version >= 6);

  free(version_reply);

  xcb_randr_get_screen_resources_cookie_t resources_cookie =
      xcb_randr_get_screen_resources(connection, screen->root);
  xcb_randr_get_screen_resources_reply_t *resources_reply =
      xcb_randr_get_screen_resources_reply(connection, resources_cookie, NULL);
  xcb_randr_output_t *xcb_outputs =
      xcb_randr_get_screen_resources_outputs(resources_reply);

  int num_outputs =
      xcb_randr_get_screen_resources_outputs_length(resources_reply);
  assert(num_outputs > 0);



  for (int i = 0; i < num_outputs; i++) {
    xcb_randr_get_output_info_cookie_t output_cookie =
        xcb_randr_get_output_info(connection, xcb_outputs[i], XCB_CURRENT_TIME);
    xcb_randr_get_output_info_reply_t *output_reply =
        xcb_randr_get_output_info_reply(connection, output_cookie, NULL);

    if (output_reply->num_modes == 0) {
      free(output_reply);
      continue;
    }

    assert(xcb_randr_get_output_info_modes_length(output_reply) > 0);

    xcb_randr_output_t ret = xcb_outputs[i];

    free(output_reply);
    free(resources_reply);

    // Just use the first output with modes
    return ret;
  }
  assert(false);
}

static VkDisplayKHR
_get_display_from_output(VkInstance instance,
                         VkPhysicalDevice physical_device,
                         Display *dpy,
                         xcb_randr_output_t output)
{
  VkResult res;

  VkDisplayKHR display;

  PFN_vkGetRandROutputDisplayEXT fun =
    (PFN_vkGetRandROutputDisplayEXT)
      vkGetInstanceProcAddr(instance, "vkGetRandROutputDisplayEXT");
  assert(fun != NULL);

  res = fun(physical_device, dpy, output, &display);
  assert(res == VK_SUCCESS);
  assert(display != VK_NULL_HANDLE);

  return display;
}

int main() {
  // Instance
  VkInstance instance;

  const char *extension_names[] = {
      VK_KHR_DISPLAY_EXTENSION_NAME,
      VK_KHR_SURFACE_EXTENSION_NAME,
      VK_EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME,
      VK_EXT_ACQUIRE_XLIB_DISPLAY_EXTENSION_NAME
  };

  VkResult res =
    vkCreateInstance (&(VkInstanceCreateInfo) {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pApplicationInfo = &(VkApplicationInfo) {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "test",
        .applicationVersion = 1,
        .engineVersion = 1,
        .apiVersion = VK_MAKE_VERSION (1, 0, 0)
      },
      .enabledExtensionCount = ARRAY_SIZE(extension_names),
      .ppEnabledExtensionNames = extension_names
    },
    NULL,
   &instance);

  assert(res == VK_SUCCESS);

  // Physical device
  uint32_t num_devices = 0;
  res = vkEnumeratePhysicalDevices(instance, &num_devices, NULL);

  assert(res == VK_SUCCESS);
  assert(num_devices > 0);

  VkPhysicalDevice *physical_devices =
    malloc(sizeof(VkPhysicalDevice) * num_devices);
  res = vkEnumeratePhysicalDevices (instance, &num_devices, physical_devices);
  assert(res == VK_SUCCESS);

  VkPhysicalDevice physical_device = physical_devices[0];

  free(physical_devices);

  // Queue props
  uint32_t num_queues = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &num_queues, NULL);

  VkQueueFamilyProperties *queue_family_props =
      malloc(sizeof(VkQueueFamilyProperties) * num_queues);

  vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &num_queues,
                                           queue_family_props);

  assert(num_queues > 0);

  uint32_t queue_index = 0;
  for (; queue_index < num_queues; queue_index++)
    if (queue_family_props[queue_index].queueFlags & VK_QUEUE_GRAPHICS_BIT)
      break;

  assert(queue_index < num_queues);

  free(queue_family_props);

  // Device
  VkPhysicalDeviceFeatures physical_device_features;
  vkGetPhysicalDeviceFeatures (physical_device, &physical_device_features);

  VkDeviceCreateInfo device_info =
    {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .queueCreateInfoCount = 1,
      .pQueueCreateInfos = (VkDeviceQueueCreateInfo[]) {
        {
          .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .queueFamilyIndex = queue_index,
          .queueCount = 1,
          .pQueuePriorities = (const float[]) { 1.0f }
        },
      },
      .pEnabledFeatures = &physical_device_features
    };

  VkDevice device;
  res = vkCreateDevice (physical_device, &device_info, NULL, &device);
  assert(res == VK_SUCCESS);

  // xcb
  // Vulkan unfortunatelty needs a Xlib connection
  Display *dpy = XOpenDisplay(NULL);
  xcb_connection_t *connection = XGetXCBConnection(dpy);
  assert(!xcb_connection_has_error(connection));

  const xcb_setup_t *setup = xcb_get_setup(connection);
  xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
  xcb_screen_t *screen = iter.data;

  xcb_randr_output_t output = _get_randr_output(connection, screen);

  VkDisplayKHR display = _get_display_from_output(instance, physical_device,
                                                  dpy, output);

  // Acquire Xlib display
  PFN_vkAcquireXlibDisplayEXT acquireXlibDisplay = (PFN_vkAcquireXlibDisplayEXT)
      vkGetInstanceProcAddr(instance, "vkAcquireXlibDisplayEXT");
  assert(acquireXlibDisplay != NULL);

  res = acquireXlibDisplay(physical_device, dpy, display);
  assert(res == VK_SUCCESS);

  // This will cause VkDisplayModeKHR leak errors
  _get_mode_properties(physical_device, display);

  // Release Xlib display
  PFN_vkReleaseDisplayEXT releaseDisplay = (PFN_vkReleaseDisplayEXT)
      vkGetInstanceProcAddr(instance, "vkReleaseDisplayEXT");
  assert(releaseDisplay != NULL);

  res = releaseDisplay(physical_device, display);
  assert(res == VK_SUCCESS);

  // Cleanup
  XCloseDisplay(dpy);
  vkDestroyDevice(device, NULL);
  vkDestroyInstance(instance, NULL);

  return 1;
}
